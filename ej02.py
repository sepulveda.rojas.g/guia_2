#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#El siguiente String está encriptado de mala manera a pesar que de todos modos sigue siendo un mensaje incomprensible.
#Idea un método para extraer el mensaje y guárdalo en una variable llamada mensaje y mostrar el valor de la variable en pantalla.
#string = ¡XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt”

#Entradas: A través del mensaje recibido debemos encontrar una solucion para saber lo que está escondido en él.
#Procedimiento: Tendremos que voltear las palabras, quitar las "x" que no son parte del mensaje final, y después crear la lista en una cadena.
#Salidas: Se le entregará el mensaje decifrado al usuario.

string = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XaX XsXiX XsXiXhXt"

mensaje_volteado = (string[::-1])
mensaje_sinx = mensaje_volteado.split("X")
mensaje_final = "".join(mensaje_sinx)


print("El mensaje a decifrar es:", string)
print("El mensaje invertido es:", mensaje_volteado)
print ("El mensaje sin x sería: ", mensaje_sinx)
print("El mensaje final es:", mensaje_final)
