#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Escriba un programa que indique si una palabra existe dentro de un texto. La palabra como la oración serán ingresadas por el usuario.

#Entradas: Se le pedirá al usuario que ingrese una oración y una palabra para buscar en ella.
#Procedimiento: Se buscará la palabra ingresada en la oración, utilizando un if/else para mostrar un mensaje más claro.
#Salidas: Se entregará un mensaje si se encontró o no la palabra en la oración ingresada.

oracion = input("Ingrese una oración cualquiera: ")
palabra = input("Ingrese la palabra que desea buscar en la oración: ")

if palabra in oracion:
    print("La palabra ingresada se encuentra en la oración")
else:
    print("La palabra ingresada no se encuentra en la oración")
