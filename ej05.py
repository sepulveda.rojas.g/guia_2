#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Entradas: Se le pedirá que ingrese dos palabras, deben ser al gusto del usuario.

#Procedimiento: A través de una operación sabremos la cantidad de letras que tendrá cada palabra ingresada, y con una resta podremos
#identificar la diferencia que hay entre cada palabra, en el caso de que existiera.

#Salidas: Se le entregará si las palabras ingresadas contienen la misma cantidad de letras o cuál es la diferencia.

#Escriba un programa que pida al usuario dos palabras, y se indique cuál de ellas es la más larga y por cuántas letras lo es.

primera_palabra = input("Ingrese la primera palabra: ")

segunda_palabra = input("Ingrese la segunda palabra: ")

if len(primera_palabra) > len(segunda_palabra):
    cantidad_caracteres = len(primera_palabra) - len(segunda_palabra)
    print("La primera palabra es mayor que la segunda palabra por", cantidad_caracteres, "letra/s")

elif len(segunda_palabra) > len(primera_palabra):
    cantidad_caracteres = len(segunda_palabra) - len(primera_palabra)
    print("La segunda palabra es mayor que la primera palabra por", cantidad_caracteres, "letra/s")

else:
    print("Ambas palabras tienen la misma cantidad de letras") 
