#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Cree un programa que permita concatenar los datos o palabras ingresadas por un usuario. El ingreso de palabras debe estar controlado 
#por un ciclo y cuando se escriba la palabra fin termine la ejecución y se muestren todos los datos ingresados de forma concatenada.

#Entradas: Se le pedirá al usuario que ingrese tantos datos quiera en el sistema, sin importar el tipo.
#Procedimiento: A través de un bucle crearemos una condición de que, mientras no ingrese la palabra "fin", todo lo que digite se ingresará al programa.
#Salidas: Se le entregará una lista de los datos digitados por el usuario.

lista=[]

valor = input("Ingresar valor (fin para finalizar): ")

while valor!= "fin" :
    lista.append(valor)
    valor = input("Ingresar valor (fin para finalizar): ")

print(" ".join(lista))