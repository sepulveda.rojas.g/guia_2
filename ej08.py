#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Realizar el juego del ahorcado.
#Primero se debe ingresar la palabra a adivinar de hasta 10 caracteres. Luego se muestra por cada letra un guion bajo 
#para que el jugador sepa la cantidad de letras a adivinar. Se irá ingresando una a una las letras y si estas se 
#encuentran en la palabra las deberá ir mostrando en el lugar correspondiente. Por cada letra que no se encuentre en la 
#palabra perderá una vida. El jugador dispondrá de 5 vidas para intentar ganar el juego.

#Entradas: Se le pedirá al usuario que ingrese una palabra al juego del ahorcado.

#Procedimiento: A través de un bucle podremos saber si las palabras que ingresamos tienen en común con la palabra secreta
#del juego, perdiendo una vida de cinco cada vez que no le atinamos a la letra que compone la palabra.

#Salidas: Depende si la ha adivinado o no, se le mostrará un mensaje de "Ha ganado" o "Ha perdido"

palabra = input("Hola, ingresa la palabra al juego: ")

print("Comienza a jugar: ")

tupalabra = " "
vidas = 5

while vidas > 0:
    fallas = 0
    for letra in palabra:

        if letra in tupalabra:
            print(letra,end="")

        else:
            print("_ ",end="")
            fallas += 1

    if fallas == 0:
        print(" ")
        print("Has ganado el juego")
        break

    tuletra=input(" Introduce una letra: ")
    tupalabra += tuletra

    if tuletra not in palabra:
        vidas -= 1
        print("Letra equivocada")
        print("Ahora tienes",+vidas," vidas")

    if vidas == 0:
        print("Perdiste el juego")

else:
    print("Gracias por jugar")