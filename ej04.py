#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Extienda el programa anterior para logre encontrar palabras dentro de un texto, sin importar si están escritas en diferente 
#combinación de mayúsculas/minúsculas.

#Entradas: Se le pedirá al usuario que ingrese una frase, y después la palabra que quiere buscar en ella, sin importar mayúsculas o minúsculas.

#Procedimiento: A través de expresiones regulares podremos crear la condición para que, sin importar el tipo de letra, nos pueda ingresar si
#existe o no en la oración.

#Salidas: Se le entregará al usuario un mensaje, dependiendo si existe o no su palabra en la oración.

import re

frase = input ("Ingrese una frase: ")
palabra = input ("Ingrese la palabra que busca: ")

if re.findall(palabra, frase, flags = re.IGNORECASE):
    print("La palabra se encuentra en la oración")
else:
    print("La palabra no se encontró")