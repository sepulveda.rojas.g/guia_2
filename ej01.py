#Nombre: Gabriela Sepúlveda Rojas 
#RUT: 20.787.581-3 
#Fecha: 10 de septiembre del 2021

#Dada un String que se forman de manera aleatoria, devuelva una nueva lista cuyo contenido sea igual a la original 
#pero de modo invertida.

#Entradas: Se le pedirá al usuario que ingrese valores para la creación de la lista, sin importar el tipo.

#Procedimiento: A través de un bucle while podremos ingresar valores hasta que el usuario ingrese la palabra "fin", para
#después leerlos de atrás hacia adelante.

# #Salidas: Se le entregará una lista invertida, según los valores ingresados.

lista = []

valor = input("Ingrese un valor (fin para finalizar): ")

while valor!= "fin" :
    lista.append(valor)
    valor = input("Ingrese un valor (fin para finalizar): ")

print ("La versión invertida de los valores ingresados por el usuario sería: ")
print(lista[::-1])